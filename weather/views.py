from django.shortcuts import render
from .models import City
from .forms import CityForm
import requests
from django.contrib import messages


def HomePage(request):
	url = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid=2cc942853fbc03b888e5a5907b166839'
	
	if request.method == 'POST':
		form = CityForm(request.POST)
		form.save(commit = False)

		name_city = form.cleaned_data['name']
		rx = requests.get(url.format(name_city)).json()

		if len(rx) < 3:
			messages.error(request,'Không tìm thấy kết quả')
		else:
			form.save()
	form = CityForm()


	cities = City.objects.all().exclude(name=None).order_by('-id','name').distinct()

	weather_data = []

	def dedupe(items,key = None):
		seen = set()
		for item in items:
			val = item if key is None else key(item)
			if val not in seen:
				yield item
				seen.add(val)
				

	for city in cities:

		r = requests.get(url.format(city)).json()
		if len(r) == 2:
			pass
		else:

			city_weather = {
				'city' : city.name,
				'temperature' : round(r['main']['temp'] -273.15,2),
				'description' : r['weather'][0]['description'],
				'icon' : r['weather'][0]['icon'],
				'id': r['id'],
 			}

			weather_data.append(city_weather)

	weather_data = list(dedupe(weather_data, key = lambda d:d['id'] ))

	content = {'weather_data':weather_data, 'form':form}

	return render(request, 'weather/weather.html', content)
